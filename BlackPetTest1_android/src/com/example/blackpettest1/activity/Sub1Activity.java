package com.example.blackpettest1.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.blackpettest1.R;
import com.example.blackpettest1.adapter.Lst1Adapter;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

public class Sub1Activity extends Activity {

	private Sub1Activity act;
	private ListView lst1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_sub1);
		this.act = this;
		
		lst1 = (ListView) findViewById( R.id.lst1 );
		
		// TODO blackpet :: sample data
		List<Map<String, String>> data = new ArrayList<Map<String,String>>();
		Map<String, String> m = new HashMap<String, String>();
		m.put( "key1", "val1" );
		m.put( "key2", "val2" );
		m.put( "key3", "val3" );
		data.add(m);
		data.add(m);
		data.add(m);
		data.add(m);
		data.add(m);
		data.add(m);
		data.add(m);
		data.add(m);
		data.add(m);
		data.add(m);
		data.add(m);
		data.add(m);
		
		lst1.setAdapter( new Lst1Adapter(act, data) );
	}
}
