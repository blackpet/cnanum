package com.example.blackpettest1.activity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.app.Activity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.example.blackpettest1.exception.CSNoResourceIdException;

public class CSBaseActivity extends Activity 
	implements OnClickListener
	, OnItemClickListener
	, OnFocusChangeListener {
	
	/**
	 * get resource entry name (android:id) from layout XML defined
	 * @param v
	 * @return
	 * @throws CSNoResourceIdException 
	 */
	private String getId( View v ) throws CSNoResourceIdException {
		String resId = v.getResources().getResourceEntryName( v.getId() );

		// not defined [android:id] throw Exception
		if( resId == null ) {
			throw new CSNoResourceIdException();
		}
		
		return resId;
	}
	
	/**
	 * override OnClickListern.onClick
	 */
	@Override
	public void onClick(View v) {
		Method method = null;
		try {
			String resId = getId( v );
			method = this.getClass().getMethod( resId + "_OnClick", new Class[] { View.class } );
			method.invoke( this, new Object[] { v } );
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CSNoResourceIdException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Method method = null;
		try {
			String resId = getId( arg0 );
			method = this.getClass().getMethod( resId + "_OnKey", new Class[] { AdapterView.class, View.class, Integer.class, Long.class } );
			method.invoke( this, new Object[] { arg0, arg1, arg2, arg3 } );
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CSNoResourceIdException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		Method method = null;
		try {
			String resId = getId( v );
			method = this.getClass().getMethod( resId + "_OnFocusChange", new Class[] { View.class, Boolean.class } );
			method.invoke( this, new Object[] { v, hasFocus } );
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CSNoResourceIdException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
