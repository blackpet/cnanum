package com.example.blackpettest1.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.blackpettest1.R;
import com.example.blackpettest1.web.CSDefaultWebViewClient;

public class Sub2Activity extends CSBaseActivity {

	private Sub2Activity act;
	private Button btnSearchBarToggle; // [검색 보이기/숨기기]
	private LinearLayout searchBar; // search bar area
	private EditText txtSearch; // 검색어 입력
	private ScrollView mainContainer;
	private WebView webProdLst;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_sub2);
		this.act = this;
		
		initComponent();
	}

	/**
	 * initialize component
	 */
	private void initComponent() {
		btnSearchBarToggle = (Button) findViewById( R.id.btnSearchBarShow );
		btnSearchBarToggle.setOnClickListener( this );
		
		searchBar = (LinearLayout) findViewById( R.id.searchBar );
		
		txtSearch = (EditText) findViewById( R.id.txtSearch );
		txtSearch.setOnFocusChangeListener( this );
		
		Button btnSearch = (Button) findViewById( R.id.btnSearch );
		btnSearch.setOnClickListener( this );
		
		// 메인영역 상단의 search bar는 페이지 초기 진입 시 보이지 않게 scroll top 지정!
		mainContainer = (ScrollView) findViewById( R.id.mainContainer );
		// View 로딩 시 EditText (txtSearch)가 focus되어 있어서 scrollTo가 원하는데로 동작하지 않는다! 강제로 focus를 옮기자!
		mainContainer.requestFocus();
		// View가 Ready되고 나서 scroll 시켜라!!
		mainContainer.post(new Runnable() {
			@Override
			public void run() {
				mainContainer.scrollTo( 0, getResources().getDimensionPixelSize(R.dimen.prod_searchbar_height) );
			}
		});
		
		webProdLst = (WebView) findViewById( R.id.webProdLst );
		webProdLst.setWebViewClient( new CSDefaultWebViewClient(webProdLst) );
		webProdLst.setWebChromeClient( new WebChromeClient() );
		webProdLst.setVerticalScrollbarOverlay( true );
		webProdLst.getSettings().setJavaScriptEnabled( true );
		webProdLst.loadUrl( "http://192.168.0.128:10070/" );
	}
	
	/**
	 * [btnSearchBarShow] onClick
	 * search bar 노출
	 */
	public void btnSearchBarShow_OnClick(View v) {
		// 상단에 scroll 되어 있는 search bar를 노출하자!
		mainContainer.smoothScrollTo( 0, 0 );
	}
	
	/**
	 * [btnSearch] onClick
	 * 검색어로 검색!!
	 * @param v
	 */
	public void btnSearch_OnClick( View v ) {
		Log.v( "BlackPet", "검색어는~~" + txtSearch.getText().toString() );
		
		// 입력한 검색어가 없으면 아무짓도 하지마라!!
		if( "".equals(txtSearch.getText().toString()) ) return;
		
		String word = txtSearch.getText().toString();
		// TODO blackpet:: 검색처리!!!
		Toast.makeText( act, "\"" + word + "\" 검색을 시작합니다!", Toast.LENGTH_LONG ).show();
	}
	
	public void txtSearch_OnFocusChange(View v, boolean hasFocus) {
		
	}
	
	@Override
	public void onBackPressed() {
		if(webProdLst.canGoBack())
			webProdLst.goBack();
		else
			super.onBackPressed();
	}
}
