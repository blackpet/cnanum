package com.example.blackpettest1.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import com.example.blackpettest1.R;

public class MainActivity extends CSBaseActivity {

	private MainActivity act = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_main);
		this.act = this;
		
		Button btn1 = (Button) findViewById(R.id.main_btn1);
		btn1.setOnClickListener( this );
		Button btn2 = (Button) findViewById(R.id.main_btn2);
		btn2.setOnClickListener( this );
		Button btn3 = (Button) findViewById(R.id.main_btn3);
		btn3.setOnClickListener( this );
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * [main_btn1] Click!!
	 * @param v
	 */
	public void main_btn1_OnClick(View v) {
		startActivity( new Intent(act, Sub1Activity.class) );
	}
	
	/**
	 * [main_btn2] Click!!
	 * @param v
	 */
	public void main_btn2_OnClick(View v) {
		startActivity( new Intent(act, Sub2Activity.class) );
	}
	
	/**
	 * [main_btn3] Click!!
	 * @param v
	 */
	public void main_btn3_OnClick(View v) {
		startActivity( new Intent(act, Sub3Activity.class) );
	}

}
