package com.example.blackpettest1.adapter;

import java.util.List;
import java.util.Map;

import com.example.blackpettest1.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class Lst1Adapter extends ArrayAdapter<Map<String, String>> {

	Context ctx;
	
	public Lst1Adapter( Context ctx, List<Map<String, String>> data ) {
		super( ctx, 0, data );
		this.ctx = ctx;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		Map<String, String> item = getItem(position);
		if( view == null ) {
			LayoutInflater li = (LayoutInflater) ctx.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
			view = li.inflate( R.layout.list_lst1, null );
		}
		TextView txt1 = (TextView) view.findViewById(R.id.txt1);
		txt1.setText( item.get("key1") );
		TextView txt2 = (TextView) view.findViewById(R.id.txt2);
		txt2.setText( item.get("key2") );
		TextView txt3 = (TextView) view.findViewById(R.id.txt3);
		txt3.setText( item.get("key3") );
		
		return view;
	}
}
