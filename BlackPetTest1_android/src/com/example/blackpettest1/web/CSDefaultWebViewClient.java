package com.example.blackpettest1.web;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public class CSDefaultWebViewClient extends WebViewClient {
	private WebView wv;
	
	public CSDefaultWebViewClient( WebView wv ) {
		this.wv = wv;
	}
	@Override
	public boolean shouldOverrideUrlLoading(WebView view, String url) {
		wv.loadUrl( url );
		
		return true;
	}

}
