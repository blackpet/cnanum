package com.example.blackpettest1.exception;

public class CSEventCallbackFailException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2290047759480324923L;

	public CSEventCallbackFailException( Throwable e ) {
		super( e );
	}
	
	public CSEventCallbackFailException( String str ) {
		super( "check for method exists {android:id}_{Event} >> " + str );
	}
	
	public CSEventCallbackFailException() {
		super();
	}
}
