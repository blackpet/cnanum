package com.example.blackpettest1.exception;

public class CSNoResourceIdException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3379316088576742462L;

	public CSNoResourceIdException( Throwable e ) {
		super( e );
	}
	
	public CSNoResourceIdException(String str) {
		super( "check for layout XML [android:id] Attribute >> " + str );
	}
	
	public CSNoResourceIdException( ) {
		super();
	}	
}
