/**
 * Hanwha Global Academy ASP Mobile
 */
package cs.hga.mobile.service;

import java.util.List;

import kr.co.codeseeds.csf.core.CSFDao;
import kr.co.codeseeds.csf.core.CSFMap;
import net.sf.json.JSONArray;

/**
 * @author blackpet
 *
 */
public class HgaMobile {
	private CSFDao dao;
	
	public void setDao( CSFDao dao ) {
		this.dao = dao;
	}
	
	/**
	 * Daily English 목록 (금일학습목록)
	 * 
	 * @param _csfmap
	 * @return
	 * @throws Exception
	 */
	public CSFMap dailyEnglishLst(CSFMap _csfmap) throws Exception {
		List<CSFMap> list = this.dao.queryForList("hgamobile.dailyEnglishLst", _csfmap);
		
		_csfmap.setJson( JSONArray.fromObject(list) );
		_csfmap.setJsonViewName();
		return _csfmap;
	}
	
	/**
	 * Daily English 상세 (금일학습컨텐츠)
	 * 
	 * @param _csfmap
	 * @return
	 * @throws Exception
	 */
	public CSFMap dailyEnglishDtl(CSFMap _csfmap) throws Exception {
		CSFMap obj = (CSFMap) this.dao.queryForObject("hgamobile.dailyEnglishDtl", _csfmap);
		
		_csfmap.setJson( JSONArray.fromObject(obj) );
		_csfmap.setJsonViewName();
		return _csfmap;
	}
}
