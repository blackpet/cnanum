package kr.co.codeseeds.util;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


/**
 * ---------------------------------------------------------Project<br>
 * Project 		: e-HRD<br>
 * System Name  : cutil
 * File Name    : DateUtil.java<br>
 * Creater		: 김강석<br>
 * Create Date	: 2004-04-19<br>
 * <br>
 * ---------------------------------------------------------Modify<br>
 * Modify Date	: Modifier : Modify Contents<br>
 * 2004-04-19	: 김강석   : create<br>
 * ...<br>
 * <br>
 * ---------------------------------------------------------Description<br>
 * Description	: 날짜관련 공통 모듈<br>
 * <br>
 * ---------------------------------------------------------JavaDocs<br>
 * @author 김강석<br>
 * @version v1.0<br>
 */

public class DateUtil
{
	/**
	 * (0 = Sunday, 1 = Monday, 2 = Tuesday, 3 = Wednesday, 4 = Thursday, 5 =
	 * Friday, 6 = Saturday) 특정일(yyyyMMdd) 에서 주어진 일자만큼 더한 날짜를 계산한다.
	 * 
	 * @param date 특정일
	 * @param rday 주어진 일자
	 * @return 더한 날짜
	 * @throws Exception
	 * modified by keegun!!!!
	 */
	public static String getRelativeDate(String date, int rday) throws Exception
	{
		if (date == null)
			return null;
		if (date.length() < 8)
			return ""; // 최소 8 자리
		String time = "";

		TimeZone kst = TimeZone.getTimeZone("JST");
		TimeZone.setDefault(kst);
		Calendar cal = Calendar.getInstance(kst); // 현재
		int yyyy = Integer.parseInt(date.substring(0, 4));
		int mm = Integer.parseInt(date.substring(4, 6));
		int dd = Integer.parseInt(date.substring(6, 8));
		cal.set(yyyy, mm - 1, dd); // 카렌더를 주어진 date 로 세팅하고
		cal.add(Calendar.DATE, rday); // 그 날짜에서 주어진 rday 만큼 더한다.
		time = new SimpleDateFormat("yyyyMMdd").format(cal.getTime());

		return time;
	}

	/**
	 * 날짜를 다양한 형태로 리턴한다. 예) getDate("yyyyMMdd"); getDate("yyyyMMddHHmmss");
	 * getDate("yyyy/MM/dd HH:mm:ss"); getDate("yyyy/MM/dd"); getDate("HHmm");
	 * 
	 * @param type
	 *                  날짜형태
	 * @return String SimpleDateFormat 
	 * @throws Exception
	 */
	public static String getDate(String type) throws Exception
	{
		if (type == null)
			return null;
		String s = "";

		s = new SimpleDateFormat(type).format(new Date());

		return s;
	}

	/**
	 * 해당날짜의 요일을 계산한다. (년월일(6자리)을 지정하는데 지정되지 않으면 default 값을 사용한다. 2000.2)
	 * 
	 * 예) getDayOfWeek("2000") -> 토 (2000/1/1) getDayOfWeek("200002") -> 화
	 * (2000/2/1) getDayOfWeek("20000225") -> 금 (2000/2/25)
	 * 
	 * @param date
	 *                  요일을 구할 날짜
	 * @return String 요일
	 */
	public static String getDayOfWeek(String date) throws Exception
	{
		if (date == null)
			return null;
		int yyyy = 0, MM = 1, dd = 1, day_of_week; // default
		String days[] = { "일", "월", "화", "수", "목", "금", "토" };

		yyyy = Integer.parseInt(date.substring(0, 4));
		MM = Integer.parseInt(date.substring(4, 6));
		dd = Integer.parseInt(date.substring(6, 8));

		Calendar cal = Calendar.getInstance();
		cal.set(yyyy, MM - 1, dd);
		day_of_week = cal.get(Calendar.DAY_OF_WEEK);
		//1(일),2(월),3(화),4(수),5(목),6(금),7(토)
		return days[day_of_week - 1];
	}

	/**
	 * 오늘의 요일을 계산한다.
	 * 
	 * @return String yyyyMMdd
	 * @throws Exception
	 */
	public static String getDayOfWeek() throws Exception
	{
		return getDayOfWeek(getDate("yyyyMMdd"));
	}
	/**
	 * 시간을 스트링으로 받어서 type 형태로 리턴한다.
	 * 
	 * 예) formatTime("1200","HH:mm") -> "12:00" formatTime("1200","HH:mm:ss") ->
	 * "12:00:00" formatTime("120003","HH:mm") -> "12:00"
	 * formatTime("120003","HH:mm ss") -> "12:00 03"
	 * 
	 * @param time
	 *                  시간
	 * @param type
	 *                  시간형태
	 * @return 시간을 스트링으로 받어서 type 형태로 리턴
	 * @throws Exception
	 */

	public static String formatTime(String time, String type) throws Exception
	{
		if (time == null || type == null)
			return null;

		String result = "";
		int hour = 0, min = 0, sec = 0, length = time.length();

		hour = Integer.parseInt(time.substring(0, 2));
		min = Integer.parseInt(time.substring(2, 4));
		sec = Integer.parseInt(time.substring(4, 6));
		Calendar cal = Calendar.getInstance();
		cal.set(0, 0, 0, hour, min, sec);
		result = (new SimpleDateFormat(type)).format(cal.getTime());

		return result;
	}

	/**
	 * 날짜(+시간)을 스트링으로 받어서 type 형태로 리턴한다.
	 * 
	 * 예) formatDate("19991201","yyy/MM/dd") -> "1999/12/01"
	 * formatDate("19991201","yyyy-MM-dd") -> "1999-12-01"
	 * formatDate("1999120112","yyyy-MM-dd HH") -> "1999-12-01 12"
	 * formatDate("199912011200","yyyy-MM-dd HH:mm ss") -> "1999-12-01 12:00 00"
	 * formatDate("19991231115959","yyyy-MM-dd-HH-mm-ss") ->
	 * "1999-12-31-11-59-59"
	 * 
	 * @param date
	 *                  날짜
	 * @param type
	 *                  날짜형태
	 * @return 날짜(+시간)을 스트링으로 받어서 type 형태로 리턴
	 * @throws Exception
	 */
	public static String formatDate(String date, String type) throws Exception
	{
		if (date == null || type == null)
			return "";
		String result = "";
		int year = 0, month = 0, day = 0, hour = 0, min = 0, sec = 0, length = date.length();

		year = Integer.parseInt(date.substring(0, 4));
		month = Integer.parseInt(date.substring(4, 6));
		// month는 Calendar에서 0 base으로 작동하므로 1 을 빼준다.
		if(date.length() >= 8)
		    day = Integer.parseInt(date.substring(6, 8));
		if(date.length() >= 10)
		    hour = Integer.parseInt(date.substring(8, 10));
		if(date.length() >= 12)
		    min = Integer.parseInt(date.substring(10, 12));
		if(date.length() >= 14)
		    sec = Integer.parseInt(date.substring(12, 14));

		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, day, hour, min, sec);
		result = (new SimpleDateFormat(type)).format(cal.getTime());

		return result;
	}

	/**
	 * 두 시간의 차이를 분으로 계산한다.
	 * 
	 * 예) getMinuteDiff("20000302","20000303") --> 3600
	 * getMinuteDiff("2000030210","2000030211") --> 60
	 * getMinuteDiff("200003021020","200003021021") --> 1
	 * getMinuteDiff("20000302102000","20000302102130") --> 1 처음 파라메터가 작은 날짜인데
	 * 만약 더 큰날짜를 처음으로 주면 음수를리턴.
	 * 
	 * @param s_start
	 *                  첫번째 시간
	 * @param s_end
	 *                  두번째 시간
	 * @return 두 시간의 차이를 분으로 계산
	 * @throws Exception
	 */
	public int getMinuteDiff(String s_start, String s_end) throws Exception
	{
		long l_start, l_end, l_gap;
		int i_start_year = 0,
			i_start_month = 1,
			i_start_day = 1,
			i_start_hour = 0,
			i_start_min = 0,
			i_start_sec = 0;
		int i_end_year = 0, i_end_month = 1, i_end_day = 1, i_end_hour = 0, i_end_min = 0, i_end_sec = 0;
		try
		{
			i_start_year = Integer.parseInt(s_start.substring(0, 4));
			i_start_month = Integer.parseInt(s_start.substring(4, 6));
			// month는 Calendar에서 0 base으로 작동하므로 1 을 빼준다.
			i_start_day = Integer.parseInt(s_start.substring(6, 8));
			i_start_hour = Integer.parseInt(s_start.substring(8, 10));
			i_start_min = Integer.parseInt(s_start.substring(10, 12));
			i_start_sec = Integer.parseInt(s_start.substring(12, 14));

			i_end_year = Integer.parseInt(s_end.substring(0, 4));
			i_end_month = Integer.parseInt(s_end.substring(4, 6));
			// month는 Calendar에서0 base으로 작동하므로 1 을 빼준다.
			i_end_day = Integer.parseInt(s_end.substring(6, 8));
			i_end_hour = Integer.parseInt(s_end.substring(8, 10));
			i_end_min = Integer.parseInt(s_end.substring(10, 12));
			i_end_sec = Integer.parseInt(s_end.substring(12, 14));

		}
		finally
		{
		}

		Calendar cal = Calendar.getInstance();
		cal.set(i_start_year, i_start_month - 1, i_start_day, i_start_hour, i_start_min, i_start_sec);
		l_start = cal.getTime().getTime();
		cal.set(i_end_year, i_end_month - 1, i_end_day, i_end_hour, i_end_min, i_end_sec);
		l_end = cal.getTime().getTime();
		l_gap = l_end - l_start;
		return (int) (l_gap / (1000 * 60));
	}


	/**
	 * 일정표에서 사용하는 반복적인 날짜 입력시 사용..
	 * @param s
	 * @param type
	 * @param gubn
	 * @param rec
	 * @return
	 * @throws Exception
	 */
	public static String getDateAdd(String s, String type, String gubn, int rec) 
		throws Exception
	{
		String result = "";
		int year=0,month=0,day=0,length=s.length();
		
		if (s == null) return null;    
		if (type == null) return null;    
		if (length != 8) return null;
		
		Calendar cal=Calendar.getInstance();
		
		try
		{
			year = Integer.parseInt(s.substring(0,4));
			month= Integer.parseInt(s.substring(4,6))-1; // month 는 Calendar 에서 0 base 으로 작동하므로 1 을 빼준다.
			day  = Integer.parseInt(s.substring(6,8));
			
			cal.set(year,month,day);
			if (gubn == "year")     cal.add (Calendar.YEAR, rec);
			if (gubn == "month")    cal.add (Calendar.MONTH, rec);
			if (gubn == "date")     cal.add (Calendar.DATE,  rec);
			if (gubn == "week")     cal.add (Calendar.WEEK_OF_MONTH,  rec);
			if (gubn == "hour")     cal.add (Calendar.HOUR_OF_DAY,  rec);
			if (gubn == "minute") 	cal.add (Calendar.MINUTE,   rec);
			
			result = (new SimpleDateFormat(type)).format(cal.getTime());
		}
		finally
		{
			cal = null;
		}
		
		return result;
	}
	
	
	/**
	 * 날짜를 입력하면 요일을 나타내준다..
	 * @param date
	 * @return
	 */
	public static int Weekday(String date)
		throws Exception
	{
		if (date==null) return -1;
		
		int yyyy=0,MM=1,dd=1,day_of_week; // default
		
		try
		{
			yyyy=Integer.parseInt(date.substring(0,4));
			MM=Integer.parseInt(date.substring(4,6));
			dd=Integer.parseInt(date.substring(6,8));
		}
		finally
		{
			// do nothing
		}
		
		Calendar cal=Calendar.getInstance();
		cal.set(yyyy,MM-1,dd);
		day_of_week=cal.get(Calendar.DAY_OF_WEEK); //1(일),2(월),3(화),4(수),5(목),6(금),7(토)
		
		return day_of_week;
	}	
	
	/**
	 * 오늘 날짜를 리턴한다...
	 * @param date
	 * @return
	 */
	public static String getToday()
		throws Exception
	{
		Calendar cal=Calendar.getInstance();
		return  (new SimpleDateFormat("yyyyMMddHHmmss")).format(cal.getTime());
	}		
	

	
}