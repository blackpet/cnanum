package kr.co.codeseeds.util;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
 
/*
 * Simple demo of JDOM
 */
public class XMLParser
{
	
	public static String XMLParserString(String xml,String key,String value){
		String result ="";
		DocumentBuilderFactory t_dbf = null;
        DocumentBuilder t_db = null;
        Document t_doc = null;
        NodeList t_nodes = null;
        Element t_element = null;
        InputSource t_is = new InputSource();
 
        try
        {
            t_dbf = DocumentBuilderFactory.newInstance();
            t_db = t_dbf.newDocumentBuilder();
            t_is = new InputSource();
            t_is.setCharacterStream(new StringReader(xml));
            t_doc = t_db.parse(t_is);
            t_nodes = t_doc.getElementsByTagName(key);
 
            for (int i = 0, t_len = t_nodes.getLength(); i < t_len; i ++){
                t_element = (Element)t_nodes.item(i);
                result = t_element.getAttribute(value);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
		return result;
		
	}
	
	public static List<String> XMLParserList(String xml,String key,String value){
		List<String> list = new ArrayList<String>();
		DocumentBuilderFactory t_dbf = null;
        DocumentBuilder t_db = null;
        Document t_doc = null;
        NodeList t_nodes = null;
        Element t_element = null;
        InputSource t_is = new InputSource();
 
        try
        {
            t_dbf = DocumentBuilderFactory.newInstance();
            t_db = t_dbf.newDocumentBuilder();
            t_is = new InputSource();
            t_is.setCharacterStream(new StringReader(xml));
            t_doc = t_db.parse(t_is);
            t_nodes = t_doc.getElementsByTagName(key);
 
            for (int i = 0, t_len = t_nodes.getLength(); i < t_len; i ++){
                t_element = (Element)t_nodes.item(i);
                list.add(t_element.getAttribute(value));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
		return list;
		
	}
}
