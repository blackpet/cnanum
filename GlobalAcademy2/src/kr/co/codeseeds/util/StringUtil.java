package kr.co.codeseeds.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * ---------------------------------------------------------Project<br>
 * Project 		: e-HRD<br>
 * System Name  : cutil
 * File Name    : StringUtil.java<br>
 * Creater		: keegun<br>
 * Create Date	: 2004-04-19<br>
 * <br>
 * ---------------------------------------------------------Modify<br>
 * Modify Date	: Modifier : Modify Contents<br>
 * 2004-04-19	: keegun   : create<br>
 * ...<br>
 * <br>
 * ---------------------------------------------------------Description<br>
 * Description	: StringUtil 모듈<br>
 * <br>
 * ---------------------------------------------------------JavaDocs<br>
 * @author keegun<br>
 * @version v1.0<br>
 */
public class StringUtil
{
	/**
	 * 	줄바꿈 처리 \n 을 <pre><BR></pre> 로 변경
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static String setHTML(String str) throws Exception {
		String returnStr = "";
		if(str == null) return "";
		
		returnStr = StringUtil.replace(str , "\n" , "<BR>");
		
		return returnStr;
	}
	
	public static String setHTML(String str, String type) throws Exception {
		String returnStr = "";
		if(str == null) return "";
		
		if( "C".equals(type) ){
		str = StringUtil.replace(str,"&", "&#38;");
		str = StringUtil.replace(str,"<", "&lt;");
		str = StringUtil.replace(str,">", "&gt;");
		str = StringUtil.replace(str,"#", "&#35;");
		str = StringUtil.replace(str,"(", "&#40;");
		str = StringUtil.replace(str,")", "&#41;");
		}
		
		returnStr = StringUtil.replace(str , "\n" , "<BR>");
		
		return returnStr;
	}
	

	public static String setHTMLStr(String str) throws Exception {
		String returnStr = "";
		if(str == null) return "";
		
		returnStr =  nvl(replace(str, "'", " &#39; "));
		returnStr = replace(returnStr, "\"", " &quot; ");
		
		return returnStr;
	}	
	
	/**
	 * 	디비에 들어갈때.. 에러방지  ' or -- 를 처리 
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static String setDB(String str) throws Exception {
		String returnStr = "";
		if(str == null) return "";
		
		returnStr = StringUtil.replace(str , "'" , "''");
		returnStr  = StringUtil.replace(returnStr , "--" , "- ");
		returnStr  = returnStr.trim();
		
		return returnStr;
	}	
	
	
	
	
	/**
	 * 대상 String이 null일 경우 ""을, null이 아닐 경우 대상 String을 return
	 * @param str 대상 스트링 
	 */
	public static String nvl(String str) throws Exception
	{
		if (str == null || "null".equals(str) )
			return "";
		else
			return  str;
	}

	/**
	 * 대상 String이 null일 경우 ""을, null이 아닐 경우 대상 String을 trim한 후 return
	 * @param str trim한 대상 스트링 
	 */
	public static String trim(String str) throws Exception
	{
		String sTmp = str;

		if (str == null)
		{
			sTmp = "";
		}
		else if (!"".equals(str) && str.length() > 0)
		{
			sTmp = str.trim();
		}

		return sTmp;
	}

	/**
	 * 대상 String이 null일 경우 ""을, null이 아닐 경우 대상 String을 trim한 후 return
	 * @param str trim한 대상 스트링 
	 * @param value null일 경우 대체 string
	 */
	public static String trim(String str, String value) throws Exception
	{
		String sTmp = str;

		if (str == null)
		{
			sTmp = value;
		}
		else if (!"".equals(str) && str.length() > 0)
		{
			sTmp = str.trim();
		}

		return sTmp;
	}

	/**
	 * 대상 String배열의 값이 null일 경우 ""를, null이 아닐 경우 원래값 그대로를 
	 * 가진 String 배열을 return
	 * @param str 대상 스트링 
	 */
	public static String[] nvl(String str[]) throws Exception
	{
		String[] new_str = null;

		new_str = new String[str.length];
		for (int i = 0; i < str.length; i++)
			new_str[i] = nvl(str[i]);

		return new_str;
	}

	/**
	 * 대상 String이 null일 경우 대체 String을, null이 아닐 경우 대상 String을 return
	 * @param str 대상 문자
	 * @param val null일 경우 대체될 문자
	 */
	public static String nvl(String str, String val) throws Exception
	{
		if (str == null)
			return val;
		else if ("".equals(str))
			return val;
		else
			return str;
	}

	/**
	 * 대상 String(str1)이 String(str2)일 경우 대체 String(val1)을, 아닐 경우 String(val2)을 return
	 * @param str 대상 문자
	 * @param val null일 경우 대체될 문자
	 */
	public static String snvl(String str1, String str2, String val1, String val2) throws Exception
	{
		if (str1.equals(str2))
			return val1;
		else
			return val2;
	}

	/**
	 * 대상 String이 null일 경우 대체 int 를, null이 아닐 경우 대상 String을 
	 * int 로 변환하여 return
	 * @param str 대상 문자
	 * @param val null일 경우 대체될 문자
	 */
	public static int nvl(String str, int val) throws Exception
	{
		int iRs = 0;

		if (str == null)
			iRs = val;
		else if( "".equals(str) )
			iRs = val;
		else
			iRs = Integer.parseInt(str);

		return iRs;

	}

	/**
	 * 대상 int가 0일경우 경우 ""을 return
	 * @param str 대상 스트링 
	 */
	public static String intToNull(int iNo) throws Exception
	{
		if (iNo == 0)
			return "";
		else
			return Integer.toString(iNo);
	}
	
	/**
	 * 대상 double가 0일경우 경우 ""을 return
	 * @param str 대상 스트링 
	 */
	public static String doubleToNull(double iNo) throws Exception
	{
		if (iNo == 0)
			return "";
		else
			return Double.toString(iNo);
	}

	/**
	 * 대상 String 중 특정한 문자가 몇번 들어가 있는지 return
	 * @param str 대상 String
	 * @param find 찾고자 하는 String
	 */
	public static int cntInStr(String str, String find) throws Exception
	{
		int i = 0;
		int pos = 0;
		while (true)
		{
			pos = str.indexOf(find, pos);
			if (pos == -1)
				break;
			i++;
			pos++;
		}

		return i;
	}

	/**
	 * String 중 start위치 부터 count만큼 잘라내서 return
	 * 주의점-> start 는 1 base 가 아니고 0 base
	 * @param str 대상 문자열
	 * @param start 시작 index
	 * @param count 잘라올 character 수
	 */
	public static String midString(String str, int start, int count) throws Exception
	{
		if (str == null)
			return null;

		String result = null;

		if (start >= str.length())
			result = "";
		else if (str.length() < start + count)
			result = str.substring(start, str.length());
		else
			result = str.substring(start, start + count);

		return result;
	}

	/**
	 * 대상 String 의 뒤에서부터 count만큼 잘라내서 return
	 * @param str 대상 문자열
	 * @param count 잘라낼 character 수
	 */
	public static String rightString(String str, int count) throws Exception
	{
		if (str == null)
			return null;

		String result = null;

		if (count == 0) // 갯수가 0 이면 공백을 
			result = "";
		else if (count > str.length()) // 문자열 길이보다 크면 문자열 전체를
			result = str;
		else
			result = str.substring(str.length() - count, str.length());
		// 오른쪽 count 만큼 리턴

		return result;
	}

	/**
	 * 대상 String 에서 특정 String을 찾아서 다른 String으로 대체하여 return
	 * @param str 대상 String
	 * @param from 찾는 String
	 * @param to 취환할 String
	 */
	public static String replace(String str, String from, String to) 
	{
		String sResult = "";
		try
		{
		    //TO는 Blank가 없수도 있습니다.
			if (str == null
				|| str.length() == 0
				|| from == null
				|| from.length() == 0
				|| to == null
				//|| to.length() == 0)
				)
				return str;

			StringBuffer sb = null;			
			
			sb = new StringBuffer(str.length() * 2);
			String posString = str.toLowerCase();
			String cmpString = from.toLowerCase();
			int i = 0;
			boolean done = false;
			while (i < str.length() && !done)
			{
				int start = posString.indexOf(cmpString, i);
				if (start == -1)
				{
					done = true;
				}
				else
				{
					sb.append(str.substring(i, start) + to);
					i = start + from.length();
				}
			}
			if (i < str.length())
			{
				sb.append(str.substring(i));
			}

			sResult = sb.toString();
		}
		catch(Exception e) {
			sResult  = str;
		}
		finally
		{
			
		}

		return sResult;
	}

	/**
	 * 대상 String 에서 < 와 > & 를 
	 * &lt; 와 &gt; 와 &amp;로 바꿔서 return
	 * @param sHTML 대상 String
	 */
	public static String replaceHtmlTag(String sHTML)  
	{
		if (sHTML == null || sHTML.trim().equals(""))
			return "";

		String sResult = "";
		StringBuffer sbResult = null;

		 
			String s = "";
			sbResult = new StringBuffer();

			for (int i = 0; i < sHTML.length(); i++)
			{
				s = sHTML.substring(i, i + 1);

				if (s.equals("<"))
				{
					sbResult.append("&lt;");
				}
				else if (s.equals(">"))
				{
					sbResult.append("&gt;");
				}
				else if (s.equals("\""))
				{
					sbResult.append("&quot;");
				}
				else if (s.equals("'"))
				{
					sbResult.append("&#39;");
				}
				else if (s.equals("&"))
				{
					sbResult.append("&amp;");
				}
				else
				{
					sbResult.append(s);
				}
			}

			return  sbResult.toString();
		
		  
	}

	/**
	 * "<" 존재 할 경우  Entity Code로 변경 
	 * @param sHTML 대상 String
	 */
	public static String replaceEntiCodeExistTag(String sHTML)  
	{
		/**
		if(sHTML != null & sHTML.indexOf("<script") != -1)
			return replaceHtmlTag(sHTML);
		else **/
		
		return replaceHtmlTag(sHTML);
	}
	
	
	/**
	 * 대상 String 을 trim 처리 한 후 < 와 > & 를 
	 * &lt; 와 &gt; 와 &amp;로 바꿔서 return
	 * @param sHTML 대상 String
	 */
	public static String replaceHtmlTagTrim(String sHTML) throws Exception
	{
		return replaceHtmlTag(trim(sHTML));
	}

	/**
	 * 대상 String 에서 ' 를 &#39; 로 바꿔서 return
	 * @param str 대상 String
	 */
	public static String replaceQuote(String str) throws Exception
	{
		return nvl(replace(str, "'", " &#39; "));
	}

	/**
	 * 대상 String 에서 " 를 &quot; 로 바꿔서 return
	 * @param str 대상 String
	 */
	public static String replaceDQuote(String str) throws Exception
	{
		return nvl(replace(str, "\"", " &quot; "));
	}

	/**
	 * 대상 String 에서 \r\n 을 <BR> 로 바꿔서 return
	 * @param str 대상 String
	 */
	public static String newlineToBr(String str) throws Exception
	{
		if (str == null || "".equals(str))
			return "&nbsp;";
		return replace(str, "\r\n", "<BR>");
	}

	/**
	 * 대상 String 에서 \r\n 을 \\r\\n 으로 바꿔서 return
	 * @param str 대상 String
	 */
	public static String newlineToChar(String str) throws Exception
	{
		if (str == null || "".equals(str))
			return "&nbsp;";
		return replace(str, "\r\n", "\\r\\n");
	}

	/**
	 * 대상 String 앞에 공백을 삽입하여 원하는 길이의 String을 return
	 * @param str 대상 String
	 * @param len 원하는 String의 길이
	 */
	public static String LeftBlanks(String str, int len) throws Exception
	{
		int i;
		StringBuffer buffer = new StringBuffer(str.trim());
		int buffLen = buffer.length();
		if (buffLen < len)
		{
			for (i = buffLen; i < len; i++)
			{
				buffer.insert(0, " ");
			}
		}

		return buffer.toString();
	}

	/**
	 * 대상 int 를 String으로 변환하여 앞에 0 을 삽입하여 원하는 길이의 String을 return
	 * @param in 대상 int
	 * @param len 원하는 String의 길이
	 */
	public static String LeftZeros(int in, int len) throws Exception
	{
		String line = Integer.toString(in);
		int i;
		StringBuffer buffer = new StringBuffer(line.trim());
		int buffLen = buffer.length();
		if (buffLen < len)
		{
			for (i = buffLen; i < len; i++)
			{
				buffer.insert(0, "0");
			}
		}

		return buffer.toString();
	}

	/**
	 * 대상 String을 int로 변환하여 return
	 * @param s 대상 String
	 */
	public static int toInt(String s) throws Exception
	{
		return Integer.parseInt(s);
	}

	/**
	 * 대상 int를 String 으로 변환하여 return
	 * @param i 대상 int
	 */
	public static String toString(int i) throws Exception
	{
		return String.valueOf(i);
	}

	/**
	 * 한글 포함한 스트링의 정확한 length 구하는 메소드
	 * @param s 한글 포함한 스트링
	 * @return length
	 */
	public static int getLength(String s) throws Exception
	{
		int strlen = 0;

		for (int j = 0; j < s.length(); j++)
		{
			char c = s.charAt(j);
			if (c < 0xac00 || 0xd7a3 < c)
				strlen++;
			else
				strlen += 2; //한글
		}
		return strlen;
	}

	/**
	 * 문자가 길경우에 특정 바이트 단위 길이로 자른다. 
	 * @param str 문자열 
	 * @param byteSize 남길 문자열의 길이 
	 * @return string 자르고 남은 문자열 
	 * @throws Exception
	 */
	public static String getTitleOfList(String str, int byteSize) throws Exception
	{
		int rSize = 0;
		int len = 0;

		if (str.getBytes().length > byteSize)
		{
			for (; rSize < str.length(); rSize++)
			{
				if (str.charAt(rSize) > 0x007F)
					len += 2;
				else
					len++;

				if (len > byteSize)
					break;
			}
			str = str.substring(0, rSize) + "...";
		}

		return str;

	}

	/**
	 * 한글 포함한 스트링의 정확한 substring
	 * @param s 한글 포함한 스트링
	 * @param begin begin
	 * @param end end
	 * @return 한글 포함한 substring
	 */
	public static String getSubString(String s, int begin, int end) throws Exception
	{
		int rlen = 0;
		int sbegin = 0;
		int send = 0;

		for (sbegin = 0; sbegin < s.length(); sbegin++)
		{
			if (s.charAt(sbegin) > 0x007f)
			{
				rlen += 2;
				if (rlen > begin)
				{
					rlen -= 2;
					break;
				}
			}
			else
			{
				rlen++;
				if (rlen > begin)
				{
					rlen--;
					break;
				}
			}
		}

		for (send = sbegin; send < s.length(); send++)
		{
			if (s.charAt(send) > 0x007f)
			{
				rlen += 2;
			}
			else
			{
				rlen++;
			}

			if (rlen > end)
				break;
		}

		return s.substring(sbegin, send);
	}

	/**
	 * StringTokenizer를 이용해서 원하는 구분자로 대상 String을 잘라서 일차원 String 배열로 리턴
	 * @param pStr 대상 String
	 * @param pDelim 구분자 String
	 * @return 구분자로 나눠진 String 배열
	 * @throws Exception
	 */
	public static String[] splitString(String pStr, String pDelim) throws Exception
	{
		if (pStr == null || pStr.length() == 0 || pDelim == null || pDelim.length() == 0)
		{
			return null;
		}

		String[] results = null;

		Vector vec = null;

		StringTokenizer stz = null;

		try
		{
			vec = new Vector();

			stz = new StringTokenizer(pStr, pDelim);

			while (stz.hasMoreElements())
			{
				String temp = stz.nextToken();

				vec.addElement(temp);
			}

			results = new String[vec.size()];
			for (int i = 0; i < results.length; i++)
			{
				results[i] = (String)vec.elementAt(i);
			}
		}
		finally
		{
			vec = null;
			stz = null;
		}

		return results;
	}

	/**
	 * 대상 String 내에 특정 String 패턴을 HTML태그로 강조
	 * @param pString 대상 String
	 * @param pPattern 강조할 String 패턴
	 * @return 강조된 String
	 * @throws Exception
	 */
	public static String getHighLighting(String pString, String pPattern) throws Exception
	{
		if (pString == null || pString.length() == 0 || pPattern == null || pPattern.length() == 0)
			return pString;

		String[] somePatterns = null;
		try
		{
			somePatterns = splitString(pPattern, " ");

			for (int i = 0; i < somePatterns.length; i++)
			{
				//System.out.println(somePatterns[i]);
				pString =
					replace(pString, somePatterns[i], "<FONT COLOR=\"red\">" + somePatterns[i] + "</FONT>");
			}
		}
		finally
		{
			somePatterns = null;
		}

		return pString;
	}

	/**
	 * 제목을 보여줄때 제한된 길이를 초과하면 뒷부분을 짜르고 "..." 으로 대치한다.
	 * @param str 대상 String
	 * @param len 나타낼 길이
	 * @return ..추가된 String
	 * @throws Exception
	 */
	public static String formatTitle(String strTarget, int iLimit) throws Exception
	{
		int rSize = 0;
		int len = 0;

		if (strTarget.getBytes().length > iLimit)
		{
			for (; rSize < strTarget.length(); rSize++)
			{
				if (strTarget.charAt(rSize) > 0x007F)
					len += 2;
				else
					len++;

				if (len > iLimit)
					break;
			}
			strTarget = strTarget.substring(0, rSize) + "...";
		}

		return strTarget;
	}

	/**
		 * 태그 제거하기
		 * 
		 * 사용 예: 
		 * String str = "<BODY>Hello <FONT FACE=\"궁서\"><B>진\"하게\"</B> 쓰>기</FONT></BODY>";
	 * System.out.println(removeTag(str));
	 *
	 *  결과:
	 *   진\"하게\" 쓰>기
	 * 
	 * @param s tag포함된 문장 
	 * @return String tag제거된 문장
	 */
	public static String getRemoveTag(String s) throws IOException 
	{
		final int iNormalState = 0;
		final int iTagState = 1;
		final int iStartTagState = 2;
		final int iEndiTagState = 3;
		final int iSingleQuotState = 4;
		final int iDoubleQuotState = 5;
		int state = iNormalState;
		int oldState = iNormalState;
		char[] chars = s.toCharArray();
		StringBuffer sb = new StringBuffer();
		char a;
		for (int i = 0; i < chars.length; i++)
		{
			a = chars[i];
			switch (state)
			{
				case iNormalState :
					if (a == '<')
						state = iTagState;
					else
						sb.append(a);
					break;
				case iTagState :
					if (a == '>')
						state = iNormalState;
					else if (a == '\"')
					{
						oldState = state;
						state = iDoubleQuotState;
					}
					else if (a == '\'')
					{
						oldState = state;
						state = iSingleQuotState;
					}
					else if (a == '/')
						state = iEndiTagState;
					else if (a != ' ' && a != '\t' && a != '\n' && a != '\r' && a != '\f')
						state = iStartTagState;
					break;
				case iStartTagState :
				case iEndiTagState :
					if (a == '>')
						state = iNormalState;
					else if (a == '\"')
					{
						oldState = state;
						state = iDoubleQuotState;
					}
					else if (a == '\'')
					{
						oldState = state;
						state = iSingleQuotState;
					}
					else if (a == '\"')
						state = iDoubleQuotState;
					else if (a == '\'')
						state = iSingleQuotState;
					break;
				case iDoubleQuotState :
					if (a == '\"')
						state = oldState;
					break;
				case iSingleQuotState :
					if (a == '\'')
						state = oldState;
					break;
			}
		}
		return sb.toString();
	}

	/**
	 * 도메인에서 HTTP 자르기 
	 * @param in
	 * @return http제거된 url
	 */
	public static String cutHTTP(String in)
	{
		if (in == null)
			return null;
		if (in.length() < 4)
			return in;

		if (in.substring(0, 7).equals("http://"))
		{
			in = in.substring(7);
			in = in.substring(in.indexOf("/"), in.length());
		}
		return in;
	}

	/**
	 * " : " 구분자로 들어오는 입력값을 따로 나누는 메소드
	 * @param input
	 * @return
	 */
	public static String Split1(String input)
	{
		// null체크
		if (input == null)
		{
			return null;
		}
		if (input.equals(""))
		{
			return null;
		}

		String result = new String();
		int part = 0;

		part = input.indexOf(":");
		result = input.substring(0, part);
		return result;
	}

	/**
	 * " : " 구분자로 들어오는 입력값을 따로 나누는 메소드
	 * @param input
	 * @return
	 */
	public static String Split2(String input)
	{
		// null체크
		if (input == null)
		{
			return null;
		}
		if (input.equals(""))
		{
			return null;
		}

		String result = new String();
		int part = 0, all = 0;

		part = input.indexOf(":");
		all = input.length();
		result = input.substring(part + 1, all);

		return result;
	}

	/**
	 * " : " 구분자로 들어오는 입력값을 따로 나누는 메소드
	 * @param input
	 * @return
	 */
	public static String[] Split(String input)
	{
		// null체크
		if (input == null)
		{
			return null;
		}
		if (input.equals(""))
		{
			return null;
		}

		String[] result = new String[5];
		int part = 0;

		for (int i = 0; i < 4; i++)
		{
			part = input.indexOf(":");
			result[i] = input.substring(0, part);
			input = input.substring(part + 1);
		}
		result[4] = input;
		return result;
	}

	/**
	 * String buffer에서 원하는 단어만 찾아 저장하는 메소드
	 * @param sentence
	 * @param findString
	 * @return String
	 * @throws Exception
	 */
	public static String findString(String sentence, String findString) throws Exception
	{
		String temp = "";

		// null체크
		if (sentence == null)
		{
			return "";
		}
		if (sentence.equals(""))
		{
			return "";
		}

		if (sentence.indexOf(findString) < 0)
		{
			return "";
		}

		temp = sentence.substring(sentence.indexOf(findString) + findString.length() + 1);

		int idx = temp.indexOf(":");

		if (idx > 0)
		{
			temp = temp.substring(0, idx - 4);
		}
		else
		{
			temp = temp.substring(0, temp.length() - 9);
		}

		return temp;
	}

	/**
	 * 년,월,일,시,분등과 관련된 HTML <option> 을 출력한다.
	 * default 값을 주면 그 값이 선택되게 한다.
	 * @param start
	 * @param end
	 * @param nDefault
	 * @return
	 */
	public static String getDateOptions(int start, int end, int nDefault)
	{
		String result = "";

		for (int i = start; i <= end; i++)
		{
			if (i < 100)
			{
				String temp = String.valueOf(i + 100);

				temp = temp.substring(1);

				if (i == nDefault)
				{
					result += "<option value=\"" + temp + "\" selected>" + temp;
				}
				else
				{
					result += "<option value=\"" + temp + "\">" + temp;
				}
			}
			else
			{
				if (i == nDefault)
				{
					result += "<option value=\"" + i + "\" selected>" + i;
				}
				else
				{
					result += "<option value=\"" + i + "\">" + i;
				}
			}
		}
		return result;
	}

	/**
	 * 숫자값을 콤마 처리 후 결과값 리턴 - 숫자형 Argement 
	 * @param ps_val
	 * @return
	 */
	public static String setComma(String ps_val)
	{
		long l_val = 0;
		if ((ps_val == null) || (ps_val.length() == 0))
			return "0";

		l_val = Long.parseLong(ps_val);
		java.text.NumberFormat s_fmt = java.text.NumberFormat.getInstance();
		String s_tmp = s_fmt.format(l_val);
		return s_tmp;
	}

	/**
	 * 숫자값을 콤마 처리 후 결과값 리턴 - Long형 Argement
	 * @param pl_val
	 * @return
	 */
	public static String setComma(long pl_val)
	{
		java.text.NumberFormat s_fmt = java.text.NumberFormat.getInstance();
		String s_tmp = s_fmt.format(pl_val);
		return s_tmp;
	}

	/**
	 * 숫자값을 콤마 처리 후 결과값 리턴 - double Argement
	 * @param pl_val
	 * @return
	 */
	public static String setComma(double pl_val)
	{
		java.text.NumberFormat s_fmt = java.text.NumberFormat.getInstance();
		String s_tmp = s_fmt.format(pl_val);
		return s_tmp;
	}

	/**
	 * subMoney
	 * @param pl_val
	 * @return String
	 */
	public static String subMoney(long pl_val)
	{
		String sl_val = "";
		sl_val = Long.toString(pl_val);

		if (!"".equals(sl_val))
		{
			if (sl_val.length() > 3)
			{
				sl_val = sl_val.substring(0, sl_val.length() - 3);
			}
		}

		java.text.NumberFormat s_fmt = java.text.NumberFormat.getInstance();
		String s_tmp = s_fmt.format(Long.parseLong(sl_val));
		return s_tmp;
	}
	
	/**
	 * subMoney
	 * @param pl_val
	 * @return String
	 */
	public static String subMoney(double pl_val)
	{
		String sl_val = "";
		sl_val = Double.toString(pl_val);

		if (!"".equals(sl_val))
		{
			if (sl_val.length() > 3)
			{
				sl_val = sl_val.substring(0, sl_val.length() - 3);
			}
		}

		java.text.NumberFormat s_fmt = java.text.NumberFormat.getInstance();
		String s_tmp = s_fmt.format(Long.parseLong(sl_val));
		return s_tmp;
	}	

	/**
	 * 문자열에서 콤마제거 후 결과값 리턴 
	 * @param ps_val
	 * @return
	 */
	public static String getDeleteComma(String ps_val)
	{
		StringTokenizer s_tok_str = new StringTokenizer(ps_val, ",");
		String s_con_str = "";
		while (s_tok_str.hasMoreTokens())
		{
			String s_tmp = s_tok_str.nextToken();
			s_con_str = s_con_str + s_tmp;
		}
		return s_con_str.trim();
	}

	/**
	 * formatMoney
	 * @param str
	 * @return String
	 */
	public static String formatMoney(String str)
	{
		double iAmount = (new Double(str)).doubleValue();
		java.text.DecimalFormat df = new java.text.DecimalFormat("###,###,###,###,###,###,###");
		return df.format(iAmount);
	}

	/**
	 * 문자열을 특정 크기로 만듬, 만약 남는 공간이 있으면 왼쪽에서부터 특정문자(cSpace)를 채움<BR>
	 * null이 입력되더라도 크기 만큼 특정문자를 채움
	 * @param strText String 문자열
	 * @param cSpace char 빈공란에 채울 특정문자
	 * @param iTotalSize int 특정 크기
	 * @param bIsLeft 왼쪽채우기
	 * @return 변경된 문자열
	 */
	public static String fixTextSize(String strText, char cSpace, int iTotalSize, boolean bIsLeft)
	{

		if (strText == null)
		{
			strText = "";
		}

		if (strText.length() < iTotalSize)
		{

			// 문자열의 크기가 특정크기보다 작을 때는 특정문자로 채움
			char[] carraySpace = new char[iTotalSize - strText.length()];
			Arrays.fill(carraySpace, cSpace);

			String strSpace = new String(carraySpace);

			if (bIsLeft)
			{
				// 왼쪽으로 공백을 채움
				return strSpace + strText;
			}
			else
			{
				// 오른쪽으로 공백을 채움
				return strText + strSpace;
			}

		}
		else
		{

			// 문자열의 크기가 특정크기보다 클때는 앞쪽의 문자열 잘라냄
			return strText.substring(strText.length() - iTotalSize, strText.length());

		}

	}

	public static String printRegisterNo(String registno)
	{
		String rtnStr = "";
		if (registno.length() >= 6)
		{
			rtnStr = registno.substring(0, 6) + "-XXXXXXX";
		}
		return rtnStr;
	}

	/**
	 * 메시지중 '\n'을 HTML에서 보여주기 위해 <BR>로 바꾼다.
	 * @param str
	 * @return String
	 */
	public static String setToBr(String str) throws IOException 
	{
		StringBuffer strb = new StringBuffer();
		for (int i = 0; i < str.length(); i++)
		{
			if (str.charAt(i) == '\n')
				strb.append("<BR>");
			else
				strb.append(str.charAt(i));
		}
		return strb.toString();
	}

	/**
	 * str로 받은 문자중 pAtStr인 문자를 pToStr로 변경하기
	 * @param str
	 * @param pAtChar
	 * @param pToStr
	 * @return String
	 */
	public static String setConvertChar(String str, char pAtChar, String pToStr) throws IOException
	{
		StringBuffer strb = new StringBuffer();
		for (int i = 0; i < str.length(); i++)
		{
			if (str.charAt(i) == pAtChar)
				strb.append(pToStr);
			else
				strb.append(str.charAt(i));
		}
		return strb.toString();
	}
	
	
	public static String setTextBox(String str , int width) throws Exception {
		StringBuffer temp = new StringBuffer();
		int cnt = 0;
		if(str != null) {
			for(int i=0 ; i < str.length() ; i++){
				temp.append(str.charAt(i));
				if(cnt == width) {
					temp.append("\n");
					cnt=0;
				}
				cnt++;
			}
		
		} else {
			temp.append("");
		}
		
		return temp.toString();
	}

	
	/**
	 * 
	 * @param sValues  대상들...
	 * @param isMaxValue   true면 제일 큰 것, false면 제일 작은 것
	 * @return
	 */
	public static String getMaxString(String[] sValues, boolean isMaxValue) {
		
			int iArrayLength = sValues.length;
			String sTarget = "";
			if(iArrayLength<=0) sTarget = "";
			else if(iArrayLength==1) sTarget=sValues[0];
			else {
				sTarget=sValues[0];
				for(int i=0;i<iArrayLength;i++) {
					if(isMaxValue) {
						if(sTarget.compareToIgnoreCase(sValues[i])==-1) sTarget=sValues[i];
					} else {
						if(sTarget.compareToIgnoreCase(sValues[i])==1) sTarget=sValues[i];
					}				
				}
			}
			return sTarget;
		}	
	
	
	
//	XSS 필터 함수 1
//	$str - 필터링할 출력값
//	$avatag - 허용할 태그 리스트 예)  $avatag = "p,br"
	public static String clearXSS(String str) throws Exception 
	{
		String avatag = "p,P,style,STYLE,br,BR";
		
		if (str == null) 
		{
			str = "";
		} 
		else 
		{
			str = str.replaceAll("<","&lt;");
			str = str.replaceAll("\0","");
			
			//허용할 태그를 지정할 경우
			if (!avatag.equals("")) 
			{
				avatag.replaceAll(" ","");
				
				String [] st = avatag.split(",");
			
				//허용할 태그를 존재 여부를 검사하여 원상태로 변환
				for(int x = 0; x < st.length; x++ ) 
				{
					str = str.replaceAll("&lt;"+st[x]+" ", "<"+st[x]+" ");
					str = str.replaceAll("&lt;"+st[x]+">", "<"+st[x]+">");
					str = str.replaceAll("&lt;/"+st[x], "</"+st[x]);
				}
		    }
		}
		
		return str;
	}
	
//	XSS 필터 함수 2
//	$str - 필터링할 출력값
//	$avatag - 허용할 태그 리스트 예)  $avatag = "p,br"
	public static String clearXSS(String str, String avatag) throws Exception 
	{
		if (str == null) 
		{
			str = "";
		} 
		else 
		{		
			str = str.replaceAll("<","&lt;");
			str = str.replaceAll("\0","");
			
			//허용할 태그를 지정할 경우
			if (!avatag.equals("")) 
			{
				avatag.replaceAll(" ","");
				
				String [] st = avatag.split(",");
			
				//허용할 태그를 존재 여부를 검사하여 원상태로 변환
				for(int x = 0; x < st.length; x++ ) 
				{
					str = str.replaceAll("&lt;"+st[x]+" ", "<"+st[x]+" ");
					str = str.replaceAll("&lt;"+st[x]+">", "<"+st[x]+">");
					str = str.replaceAll("&lt;/"+st[x], "</"+st[x]);
				}
		    }
		}
		
		return str;
	}
	
	/**
	 * 날짜 포맷 변경
	 * @param org 8자리 대상문자열
	 * @param delimeter 구분자
	 * @return 날짜포맷으로 변경된 문자열
	 */
	public static String getDtFormatStr(String org, String delimeter){
		String result = null;
		if( org == null ){
			return null;
		}
		if( org.length() < 8 ){
			return org;
		}
		result = org.substring(0, 4) + delimeter + org.substring(4, 6) + delimeter + org.substring(6);
		return result;
	}

	public static String convertArrayToStringWithComma(String[] arrStr){
		String returnStr = "";
		for(int i =0 ; i < arrStr.length; i++){
			returnStr+=arrStr[i];
			if(i != arrStr.length - 1){
				returnStr+=",";
			}
		}
		return returnStr;
	}
	
	/**
	 * Unicode decoding ( unescape )
	 * @param inp
	 * @return
	 */
	public static String unescape( String inp ){
		  StringBuffer rtnStr = new StringBuffer();
		  char[] arrInp = inp.toCharArray();
		  int i;
		  for( i = 0 ; i < arrInp.length ; i++ ){
		   if( arrInp[i] == '%'){
		     String hex;
		     if( arrInp[i+1] == 'u'){
		         hex = inp.substring(i+2,i+6);
		         i += 5;
		     }else{ 
			 hex = inp.substring(i+1, i+3);
		         i+=2;
		     }
		     try{
		     }catch( NumberFormatException e){
			rtnStr.append("%");
		        i =- (hex.length() > 2 ? 5 : 2 );
		     }
		   }else{
		     rtnStr.append(arrInp[i]);
		   }
		  }
		  return rtnStr.toString();
		}
	
	public static String getFileExt(String fName) {
    	int lIdx = fName.lastIndexOf(".");
    	String ext = "";
	    if (lIdx>0) ext = fName.substring(lIdx+1);
	    return ext;
	}	
	public static String html4FCKEditor(String str) {
		String ret = NVL(str);
		try {
//			ret = Util.html4HTML(ret);
			ret = replace(ret, "\"", "&quot;");
			ret = replace(ret, "&#39", "\\&#39");
			ret = replace(ret, "&#34", "\\&#34");
			ret = replace(ret, "<",    "&lt;");
			ret = replace(ret, ">",    "&gt;");
		} catch (Exception e) {
			ret = str;
		}
		return ret;
	}	
	
	public static String NVL(String val) {
		return (val==null) ? "" : val;
	}
	public static String NVL(String val, String nullVal) {
		return (val==null) ? nullVal : val;
	}	
	
	
	public static Map<String, String> getUrlParameters(String url)
	        throws UnsupportedEncodingException {
	    Map<String, String> params = new HashMap<String,String>();
	    String[] urlParts = url.split("\\?");
	    if (urlParts.length > 1) {
	        String query = urlParts[1];
	        for (String param : query.split("&")) {
	            String pair[] = param.split("=");
	            String key = URLDecoder.decode(pair[0], "UTF-8");
	            String value = "";
	            if (pair.length > 1) {
	                value = URLDecoder.decode(pair[1], "UTF-8");
	            }
	            params.put(key, value);
	        }
	    }
	    return params;
	}

public static void main(String[] arg) {
	System.out.println(" <html></html>".indexOf("2"));
}
}

