package cs.util;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

	/**
	 * extract {key, value} from querystring
	 * @param query
	 * @return
	 */
	public static Map<String, String> queryToMap( String query ) {
		Map<String, String> map = new HashMap<String, String>();
		
		Pattern p = Pattern.compile( "([^\\?&=]+)=([^&#]+)" );
		Matcher m = p.matcher( query );
		
		while( m.find() ) {
			/* Debuging
			System.out.println("0:" + m.group(0) + "\n1:" + m.group(1) + "\n2:" + m.group(2) + "\n---------------");
			*/
			map.put( m.group(1), m.group(2) );
		}
		return map;
	}
}
