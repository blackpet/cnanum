package cs.util;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import jxl.Cell;
import jxl.Range;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 * ---------------------------------------------------------Project<br>
 * Project 		: e-HRD<br>
 * System Name  : cutil
 * File Name    : ExcelUtil.java<br>
 * Creater		: keegun<br>
 * Create Date	: 2004-04-19<br>
 * <br>
 * ---------------------------------------------------------Modify<br>
 * Modify Date	: Modifier : Modify Contents<br>
 * 2004-04-19	: keegun   : create<br>
 * ...<br>
 * <br>
 * ---------------------------------------------------------Description<br>
 * Description	: Excel 공통 모듈<br>
 * <br>
 * ---------------------------------------------------------JavaDocs<br>
 * @author keegun<br>
 * @version v1.0<br>
 */
public class ExcelUtil
{

	/**
	 * 생성자
	 */
	public ExcelUtil() {}

	/**
	 * 엑셀파일 유효성 체크
	 * @param workbook 엑셀 핸들링하기 위한 workbook
	 * @param valid_col_cnt 원하는 엑셀파일 포맷의 컬럼 수
	 * @param required_col_cnt 꼭 채워져있어야할 컬럼 수(0부터 required_col_cnt 컬럼까지는 데이터가 꼭 있어야한다.)
	 * @return 체크 결과 int
	 *	 	 0 보다 크면 유효한 로우의 맥스 넘버.
	 *	 	 0 이면 빈 엑셀파일임.
	 *  	-2 이면 컬럼수가 형식에 맞지 않음.
	 *  	-3 이면 빈 컬럼이 있는 행이 있음.
	 *  	-4 이면 중간에 빈 행이 있음.
	 *  	-1 이면 로직 에러.
	 */
	public static int checkExcelFileValidity( Workbook workbook, int valid_col_cnt,
			int required_col_cnt ) throws Exception {
		// 엑셀파일 핸들링
		Sheet sheet = null;
		Cell cell = null;

		// 계속 진행 여부
		int f_keep_go = 1;

		// 중간에 빈행 존재 여부
		int f_middle_empty = 0;

		// 유효성 체크 결과
		int result = 0;

		// 한 로우의 유효성 체크
		int check_row = 0;

		// 필수 컬럼 유효성 체크
		int essential_col_cnt = 0;

		try {
			sheet = workbook.getSheet( 0 );
			String tt = "";

			if( sheet.getRows() == 0 && sheet.getColumns() == 0 ) {
				result = 0;
			} else {
				for( int i = 0; i < sheet.getRows(); i++ ) {
					if( f_keep_go == 1 ) {
						for( int j = 0; j < sheet.getColumns(); j++ ) {
							cell = sheet.getCell( j, i );
							tt = cell.getContents().trim();

							if( j < required_col_cnt ) {
								if( tt.equals( "" ) ) {
								} else {
									if( f_middle_empty == 1 ) {
										f_keep_go = 0;
										result = -4; // 중간에 빈 행이 있음.
										break;
									} else {
										essential_col_cnt++;
										check_row++;
									}
								}
							} else if( j >= valid_col_cnt ) {
								if( !tt.equals( "" ) ) {
									f_keep_go = 0;
									result = -2; // 컬럼수가 형식에 맞지 않음.
									break;
								} else {
								}
							} else {
								if( tt.equals( "" ) ) {
								} else {
									if( f_middle_empty == 1 ) {
										f_keep_go = 0;
										result = -4; // 중간에 빈 행이 있음.
										break;
									} else {
										check_row++;
									}
								}
							}
						}

						if( f_keep_go == 1 ) {
							if( check_row == 0 ) {
								f_middle_empty = 1;
							} else if( essential_col_cnt == required_col_cnt ) {
								result = i + 1;
							} else {
								f_keep_go = 0;
								result = -3; // 필수 데이터가 없음.
								break;
							}
						}

						check_row = 0;
						essential_col_cnt = 0;
					} else {
						break;
					}
				}
			}
		} finally {
			sheet = null;
			cell = null;
		}

		return result;
	}

	/**
	 * 엑셀파일 Export
	 * @param filePathName
	 *            생성될 Excel 파일의 경로와 이름
	 * @param sheetName
	 *            생성될 Excel 파일의 sheet 이름
	 * @param title
	 *            각 컬럼의 타이틀
	 * @param data
	 *            실제 데이터
	 * @return Export 결과 int 1 이면 정상종료 0 이면 뭔가 에러. -1 이면 filePathName에 문제있음. -2
	 *         이면 sheetName에 문제있음. -3 이면 title에 문제있음. -4 이면 data에 문제있음.
	 */
	public static int exportExcelFileOneSheet( String filePathName, String sheetName,
			String[] title, String[][] data ) throws Exception {
		// 엑셀파일 핸들링
		WritableWorkbook workbook = null;
		WritableSheet sheet = null;
		Cell cell = null;
		Label label = null;

		int result = 0;

		// input arg 유효성 검사
		if( filePathName == null || filePathName.equals( "" ) ) {
			result = -1;
		} else if( sheetName == null || sheetName.equals( "" ) ) {
			result = -2;
		} else if( title == null || title.length <= 0 ) {
			result = -3;
		} else if( data == null || data.length <= 0 ) {
			result = -4;
		} else {
			try {
				// 파일 열기
				workbook = Workbook.createWorkbook( new File( filePathName ) );

				// sheet 만들기
				sheet = workbook.createSheet( sheetName, 0 );

				// 제일 첫째행에 타이틀 넣기
				for( int i = 0; i < title.length; i++ ) {
					label = new Label( i, 0, title[i] );
					sheet.addCell( label );
				}

				// 엑셀파일 각 셀에 쓰기
				for( int i = 0; i < data.length; i++ ) {
					for( int j = 0; j < data[i].length; j++ ) {
						label = new Label( j, ( i + 1 ), data[i][j] );
						sheet.addCell( label );
					}
				}

				// 엑셀 파일에 쓰기
				workbook.write();

				result = 1;
			} finally {
				workbook.close();
				sheet = null;
				cell = null;
				label = null;
			}
		}

		return result;
	}

	/**
	 * 엑셀파일을 읽어서 이차원 String 배열에 넣어 return
	 * @param filePathName
	 *            읽어들일 Excel 파일의 경로와 이름
	 * @param valid_col_cnt
	 *            원하는 엑셀파일 포맷의 컬럼 수
	 * @param required_col_cnt
	 *            꼭 채워져있어야할 컬럼 수(0부터 required_col_cnt 컬럼까지는 데이터가 꼭 있어야한다.)
	 * @return 읽어들인 결과 String 배열, 에러발생시 null을 return
	 */
	public static String[][] readExcelFile( String pathName, int valid_col_cnt, int required_col_cnt )
			throws Exception {

		String sql = "";

		// 엑셀파일 핸들링
		Workbook workbook = null;
		Sheet sheet = null;
		Cell cell = null;

		Vector resultVector = null;
		String[] data = null;
		String[][] resultSet = null;

		try {
			workbook = Workbook.getWorkbook( new File( pathName ) );

			int valid_row = checkExcelFileValidity( workbook, valid_col_cnt, required_col_cnt );

			if( valid_row > 0 ) {
				sheet = workbook.getSheet( 0 );

				resultVector = new Vector();
				for( int i = 0; i < valid_row; i++ ) {
					data = new String[valid_col_cnt];

					for( int j = 0; j < valid_col_cnt; j++ ) {
						if( j < sheet.getColumns() )
							data[j] = sheet.getCell( j, i ).getContents().trim();
						else
							data[j] = "";
					}

					resultVector.addElement( data );
					data = null;
				}

				resultSet = new String[resultVector.size()][valid_col_cnt];
				for( int i = 0; i < resultSet.length; i++ ) {
					resultSet[i] = (String[])resultVector.elementAt( i );
				}
			} else {
				switch( valid_row ) {
				/**
				case 0 : // 빈 엑셀파일 인 경우
					throw new EhrdEmptyExcelFileException();
				case -2 : // 컬럼 숫자가 형식에 안맞는 경우
					throw new EhrdInvalidColumnCountException();
				case -3 : // 필수 컬럼이 비어있는 경우
					throw new EhrdVoidColumnException();
				case -4 : // 중간에 빈 행이 있는 경우
					throw new EhrdVoidRowException();
				default : // 로직 에러
					throw new Exception();
				**/
				}
				
			}

		} finally {
			data = null;
			resultVector = null;

			workbook = null;
			sheet = null;
			cell = null;
		}

		return resultSet;
	}
	

	/**
	 * 엑셀파일을 읽어서 이차원 String 배열에 넣어 return
	 * @param filePathName
	 *            읽어들일 Excel 파일의 경로와 이름
	 * @param valid_col_cnt
	 *            원하는 엑셀파일 포맷의 컬럼 수
	 * @param required_col_cnt
	 *            꼭 채워져있어야할 컬럼 수(0부터 required_col_cnt 컬럼까지는 데이터가 꼭 있어야한다.)
	 * @return 읽어들인 결과 String 배열, 에러발생시 null을 return
	 */
	public static Map readExcelFile2( String pathName, int valid_col_cnt, int required_col_cnt )
			throws Exception {

		String sql = "";

		// 엑셀파일 핸들링
		Workbook workbook = null;
		Sheet sheet = null;
		Cell cell = null;

		Vector resultVector = null;
		String[] data = null;
		String[][] resultSet = null;

		Map resultMap = new HashMap();
		try {
			workbook = Workbook.getWorkbook( new File( pathName ) );

			int valid_row = checkExcelFileValidity( workbook, valid_col_cnt, required_col_cnt );

			if( valid_row > 0 ) {
				sheet = workbook.getSheet( 0 );

				resultVector = new Vector();
				for( int i = 0; i < valid_row; i++ ) {
					data = new String[valid_col_cnt];

					for( int j = 0; j < valid_col_cnt; j++ ) {
						if( j < sheet.getColumns() )
							data[j] = sheet.getCell( j, i ).getContents().trim();
						else
							data[j] = "";
					}

					resultVector.addElement( data );
					data = null;
				}

				resultSet = new String[resultVector.size()][valid_col_cnt];
				for( int i = 0; i < resultSet.length; i++ ) {
					resultSet[i] = (String[])resultVector.elementAt( i );
				}
				resultMap.put("data", resultSet);
				
				Range[] arrayRange = sheet.getMergedCells();
				Map[][] rangeMap = new Map[resultVector.size()][valid_col_cnt];
				if ( arrayRange != null && arrayRange.length > 0 ) {
					for ( int i = 0; i < arrayRange.length; i++ ) {
						int startRow = arrayRange[i].getTopLeft().getRow();
						int startColumn = arrayRange[i].getTopLeft().getColumn();
						int endRow = arrayRange[i].getBottomRight().getRow();
						int endColumn = arrayRange[i].getBottomRight().getColumn();
						rangeMap[startRow][startColumn] = new HashMap();
						rangeMap[startRow][startColumn].put("endRow", endRow);
						rangeMap[startRow][startColumn].put("endColumn", endColumn);
					}
				}
				resultMap.put("range", rangeMap);
				
				resultMap.put("sheetName", sheet.getName());
			} else {
				switch( valid_row ) {
				/**
				case 0 : // 빈 엑셀파일 인 경우
					throw new EhrdEmptyExcelFileException();
				case -2 : // 컬럼 숫자가 형식에 안맞는 경우
					throw new EhrdInvalidColumnCountException();
				case -3 : // 필수 컬럼이 비어있는 경우
					throw new EhrdVoidColumnException();
				case -4 : // 중간에 빈 행이 있는 경우
					throw new EhrdVoidRowException();
				default : // 로직 에러
					throw new Exception();
				**/
				}
				
			}

		} finally {
			data = null;
			resultVector = null;

			workbook = null;
			sheet = null;
			cell = null;
		}

		return resultMap;
	}
}
