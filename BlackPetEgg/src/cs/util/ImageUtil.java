package cs.util;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
/**
 * 썸네일 만들기 등 여러가지 이미지 처리 
 * @프로젝트명 cspms
 * @패키지명 kr.co.codeseeds.util
 * @작성자 gwangrak hong
 * @자바파일명 ImageUtil.java
 * @작성일 2012. 8. 8.
 */
public class ImageUtil {

	public static void createImage(String loadFile, String saveFile, int zoom)
			throws IOException {
		File save = new File(saveFile);
		FileInputStream fis = new FileInputStream(loadFile);
		BufferedImage im = ImageIO.read(fis);

		if (zoom <= 0)
			zoom = 1;

		int width = 100;// im.getWidth() / zoom;
		int height = 100;// im.getHeight() / zoom;

		BufferedImage thumb = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = thumb.createGraphics();

		g2.drawImage(im, 0, 0, width, height, null);
		ImageIO.write(thumb, "png", save);

	}

	public static void main(String args[]) {
		String loadFile = "c://Tulips.png";
		String saveFile = "c://Tulips100.png";
		int zoom = 5;

		try {
			ImageUtil.createImage(loadFile, saveFile, zoom);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
