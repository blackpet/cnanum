package cs.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

public class DateJsonValueProcessor implements JsonValueProcessor{
	private final DateFormat defaultDateFormat;
	
	public DateJsonValueProcessor(){		
		defaultDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);		
	}
	public DateJsonValueProcessor(String pattern){
		defaultDateFormat = new SimpleDateFormat(pattern, Locale.KOREA);		
	}
	
	public Object processArrayValue(Object paramObject, JsonConfig arg1) {
        if(paramObject == null) return null;
        return defaultDateFormat.format(paramObject);
	}

	public Object processObjectValue(String paramString, Object paramObject, JsonConfig paramJsonConfig) {
		return processArrayValue(paramObject, paramJsonConfig);
	}

}
