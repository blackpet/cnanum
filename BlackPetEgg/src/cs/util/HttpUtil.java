package cs.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUtil {
	// POST HttpURLConnection - multipart request를 제외한 커넥션 처리.
	public static String sendRequest( String url, String param ) throws Exception{
		URL requestURL = new URL( url  );
		HttpURLConnection huc = (HttpURLConnection)requestURL.openConnection();
		huc.setDoOutput(true);
		huc.setRequestMethod("POST");
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(huc.getOutputStream()));
		bw.write(param);
		bw.flush();
		bw.close();
		
		// 결과 xml InputStream으로 받아서 처리.
		BufferedReader br = new BufferedReader(new InputStreamReader(huc.getInputStream()));
		String inputData;
		StringBuffer sb = new StringBuffer(); 
		while((inputData = br.readLine())!= null){
			sb.append(inputData);
		}
		return sb.toString();
	}
}
