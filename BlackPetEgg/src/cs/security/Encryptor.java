/**
 * 
 */
package cs.security;

import java.util.Map;

import kr.co.codeseeds.csf.core.CSFMap;

import org.apache.commons.codec.binary.Base64;

import cs.util.StringUtil;

/**
 * @author blackpet
 *
 */
public class Encryptor {

	/**
	 * decode Base64 for parameter "q"
	 * @param _csfmap
	 * @return
	 */
	public static CSFMap decodeBase64Q( CSFMap _csfmap ) {
		String q = _csfmap.getString( "q" );
		// not exists "q" parameter, return null
		if( q == null )
			return null;
		
		q = new String( Base64.decodeBase64( q ) );
		
		// extract {key, value} from querystring
		Map<String, String> map = StringUtil.queryToMap( q );
		
		// re-store parameters
		_csfmap.putAll( map );
		
		return _csfmap;
	}
	
	public static void main(String[] args ) {
		String q64 = new String( Base64.encodeBase64("aaa=bbb&ccc=ddd&eee=fff#&aaa".getBytes()) );
		System.out.println( "[BASE64 Encoded String] " + q64 );
		CSFMap map = new CSFMap();
		map.put("q", q64);
		
		map = Encryptor.decodeBase64Q(map);
		System.out.println("[result] aaa=" + map.getString("aaa"));
	}
}
