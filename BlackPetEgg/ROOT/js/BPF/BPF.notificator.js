/**
 * BPF.notificator (BlackPet Framework)
 * 
 * @author BlackPet
 * @version 0.1
 * @date 23th July, 2013
 * @required jQuery-1.8.js over / BPF.core.js
 */

(function( $ ) {
	if( typeof BPF == 'undefined' ) return null;
	
	BPF.notificator = $.extend( {}, {
		/**
		 * show notification bar like "warning" (slide down from top)
		 * 
		 * @param href uri
		 * @param form (Options) FORM Object(by name) (for passed parameters)
		 */
		warning: function( msg ) {
			var tick = new Date().getTime();
			var msg = $( '<div id="BPF_notificator_' +tick+ '" class="BPF__notificator"><div>' +msg+ '</div></div>' );
			$( 'BODY' ).append( msg );
			msg.animate( {top: $(window).scrollTop() + 50}, 'fast' )
				.delay( 2000 )
				.fadeOut( 1000, function() {
					BPF.log( this );
					this.remove();
				} );
		}, 
	} );
	
	
})( jQuery );