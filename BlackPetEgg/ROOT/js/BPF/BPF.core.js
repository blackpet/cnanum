/**
 * BPF (BlackPet Framework)
 * 
 * @author BlackPet
 * @version 0.1
 * @date 7th Jun, 2013
 * @required jquery-1.8.js over
 */

var BPF = $.extend( BPF, { version: '0.1' } );

( function( $, bpf ) {
	this.bpf = $.extend( bpf, {
		
		/**
		 * logging for browser exists {console}
		 */
		log: function( v ) {
			if( typeof console != 'undefined' && console.log ) {
				console.log( v );
			}
		}
	} );
} )( jQuery, BPF );