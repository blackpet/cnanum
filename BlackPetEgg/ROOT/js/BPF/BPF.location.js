/**
 * BPF.location (BlackPet Framework)
 * 
 * @author BlackPet
 * @version 0.1
 * @date 7th Jun, 2013
 * @required jQuery-1.8.js over / BPF.core.js
 */

(function( $ ) {
	if( typeof BPF == 'undefined' ) return null;
	
	BPF.location = $.extend( {}, {
		/**
		 * link (location.href) serialized form
		 * 
		 * @param href uri
		 * @param form (Options) FORM Object(by name) (for passed parameters)
		 */
		href: function( href, form ) {
			var uri = this.serialize( href, form );
			BPF.log( uri );
			location.href = uri;
		}, 
		
		/**
		 * historiness link (location.replace) serialized form
		 * 
		 * @param href uri
		 * @param form (Options) FORM Object(by name) (for passed parameters)
		 */
		replace: function( href, form ) {
			var uri = this.serialize( href, form );
			BPF.log( uri );
			location.replace( uri );
		},
		
		serialize: function( href, form ) {
			if( typeof form == 'undefined' || !form ) return href;
			
			// separate queryString from uri
			var href2 = href.split( '?' );
			href = href2[0];
			var query = href2.length > 1 ? href2[1] : '';
			
			for( var i = 0, el = form.elements[i]; i < form.elements.length; i++, el = form.elements[i] ) {
				var re = new RegExp( '([\\?&]?(' + el.name + ')=)([^&#])*' );
				if( re.test(query) ) {
					// rewrite exists {name} key
					query = query.replace( re, '$2=' + el.value );
				} else {
					// append {name} key
					query = query.length == 0 ? el.name + '=' + el.value
							: query + '&' + el.name + '=' + el.value;
				}
			}
			return href + '?' + query;
		}
	} );
	
	
})( jQuery );